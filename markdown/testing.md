Some stuff

A table:

| Date | Description | Change reference |
| ---------- | ---------------- | ------------------------ |
| 29-Sep-22 | Initial Version    | Portfolio 0860                  |

Done

<details><summary>Initial Load and DCRs</summary> 

> [Describe any initial load and/or DCRs (data fixes) that have been carried out, including why it was necessary and when it was done. It may be useful to follow the same structure as used in the Processing Components subsections to describe the inputs, outputs, and processing carried out by the inital load or each DCR.]: # 

Initial DCRs will be created for each table (3 DCRs) load the data from DB_UAT versions of the tables into DB_PROD. 

</details> 


With table:

<details><summary>Change History</summary>



> [A history of significant changes or additions to the product. The change reference would typically be a portfolio reference number, or a Service Now ticket number, and the Github tag associated with the change.]: #



| Date | Description | Change reference |
| ---------- | ---------------- | ------------------------ |
| 29-Sep-22 | Initial Version    | Portfolio 0860                  |



</details>
