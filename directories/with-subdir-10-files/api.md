
# API Documentation

## Overview
This API allows users to interact with our system in order to retrieve information about products and services. It is designed to be easy to use and understand, while still providing all the necessary functionality.

## Authentication
Authentication is required in order to access the API. To authenticate, use the following credentials:

* **Username**: your_username
* **Password**: your_password

## Endpoints
The API has the following endpoints available:

* `/products`: Retrieves a list of all available products
* `/services`: Retrieves a list of all available services
* `/product/<id>`: Retrieves detailed information about a specific product, given its ID
* `/service/<id>`: Retrieves detailed information about a specific service, given its ID

## Requests
To make a request to the API, use the following format:

`<method> <endpoint>`

Where `<method>` is the type of request you want to make (e.g. `GET`, `POST`, etc.) and `<endpoint>` is the endpoint you want to access (e.g. `/products`).

## Responses
The API will respond with a JSON object containing the requested information. The structure of the response will depend on the endpoint being accessed.

## Error Handling
If an error occurs while making a request to the API, a JSON object will be returned containing an `error` field with a description of the error.

## Admin Endpoints
Here is the list of our admin enpoints:

* `/admin/products/create`: Creates a new product
* `/admin/products/update/<id>`: Updates an existing product, given its ID
* `/admin/products/delete/<id>`: Deletes an existing product, given its ID
* `/admin/services/create`: Creates a new service
* `/admin/services/update/<id>`: Updates an existing service, given its ID
* `/admin/services/delete/<id>`: Deletes an existing service, given its ID
